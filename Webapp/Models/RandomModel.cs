﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace Webapp.Models
{
    public class RandomModel
    {
        static HttpClient client = new HttpClient();
        static Random r = new Random();
        public int RandomCall()
        {
            int x = r.Next(1, 10);
            var t=   client.GetStringAsync($"https://httpbin.org/delay/{x}");
            t.Wait();
            return x;
        }
    }
}