﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webapp.Models;

namespace Webapp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult RandomSlow()
        {
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            RandomModel m = new RandomModel();
            int x = m.RandomCall();
            sw.Stop();

            ViewBag.X = x;
            ViewBag.MS = sw.ElapsedMilliseconds;
            ViewBag.RequestTime = sw.ElapsedMilliseconds / 1000;

            return View();
            
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}